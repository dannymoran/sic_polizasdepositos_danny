unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,ULicencia, Vcl.ExtDlgs;

type
  TFPrincipal = class(TForm)
    btnDialog: TButton;
    txtArchivo: TEdit;
    btnActivar: TButton;
    odArchivo: TOpenTextFileDialog;
    svArchivo: TSaveTextFileDialog;
    procedure btnDialogClick(Sender: TObject);
    procedure btnActivarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    archivo: TextFile;
    lic : TLicencia;
  end;

var
  FPrincipal: TFPrincipal;

implementation

{$R *.dfm}

procedure TFPrincipal.btnActivarClick(Sender: TObject);
var
  txtcont,descifrado,salida : string;
  contrallave : Textfile;
begin
  if txtArchivo.Text <> '' then
  begin
    assignFile(archivo,odArchivo.FileName);
    Reset(archivo);
    Read(archivo,txtcont);
    descifrado := lic.DecryptStr(txtcont,lic.stringtoword('asic'));
    salida := lic.EncryptStr(descifrado.Split([';'])[1]+';'+descifrado.Split([';'])[0]+
                              ';'+descifrado.Split([';'])[2],lic.StringToWord('enc'));
    if svArchivo.Execute then
    begin
      AssignFile(contrallave,svArchivo.FileName+'.sic');
      ReWrite(contrallave);
      Write(contrallave,salida);
      CloseFile(contrallave);
    end;
  end;
end;

procedure TFPrincipal.btnDialogClick(Sender: TObject);
begin
  if odArchivo.Execute then
    txtArchivo.Text := odArchivo.FileName;
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
begin
  lic := TLicencia.Create;
end;

end.
