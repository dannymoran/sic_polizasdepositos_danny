object FProveedores: TFProveedores
  Left = 0
  Top = 0
  Caption = 'Proveedores'
  ClientHeight = 546
  ClientWidth = 831
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grdProveedores: TDBGrid
    AlignWithMargins = True
    Left = 3
    Top = 60
    Width = 320
    Height = 483
    Margins.Top = 60
    Align = alLeft
    DataSource = dsClientes
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object gbCuentas: TGroupBox
    AlignWithMargins = True
    Left = 337
    Top = 20
    Width = 491
    Height = 523
    Margins.Top = 20
    Align = alRight
    Caption = 'Cuentas'
    TabOrder = 1
    object Label8: TLabel
      Left = 24
      Top = 75
      Width = 51
      Height = 13
      Caption = 'Descuento'
    end
    object Label9: TLabel
      Left = 24
      Top = 43
      Width = 88
      Height = 13
      Caption = 'Cuenta Proveedor'
    end
    object Label10: TLabel
      Left = 24
      Top = 424
      Width = 22
      Height = 13
      Caption = 'IEPS'
    end
    object gbCompras: TGroupBox
      Left = 24
      Top = 104
      Width = 438
      Height = 161
      Caption = 'Compras'
      TabOrder = 0
      object Label1: TLabel
        Left = 40
        Top = 52
        Width = 17
        Height = 13
        Caption = '0%'
      end
      object Label2: TLabel
        Left = 40
        Top = 81
        Width = 23
        Height = 13
        Caption = '16%'
      end
      object Label3: TLabel
        Left = 40
        Top = 109
        Width = 17
        Height = 13
        Caption = '8%'
      end
      object Label6: TLabel
        Left = 168
        Top = 30
        Width = 35
        Height = 13
        Caption = 'Cr'#233'dito'
      end
      object Label7: TLabel
        Left = 304
        Top = 30
        Width = 41
        Height = 13
        Caption = 'Contado'
      end
      object txtCompras0credito: TEdit
        Left = 125
        Top = 49
        Width = 121
        Height = 21
        TabOrder = 0
        OnExit = txtCompras0creditoExit
        OnKeyDown = txtCompras0creditoKeyDown
      end
      object txtCompras16Credito: TEdit
        Left = 125
        Top = 76
        Width = 121
        Height = 21
        TabOrder = 2
        OnExit = txtCompras16CreditoExit
        OnKeyDown = txtCompras16CreditoKeyDown
      end
      object txtCompras0Contado: TEdit
        Left = 264
        Top = 49
        Width = 121
        Height = 21
        TabOrder = 1
        OnExit = txtCompras0ContadoExit
        OnKeyDown = txtCompras0ContadoKeyDown
      end
      object txtCompras16Contado: TEdit
        Left = 264
        Top = 76
        Width = 121
        Height = 21
        TabOrder = 3
        OnExit = txtCompras16ContadoExit
        OnKeyDown = txtCompras16ContadoKeyDown
      end
      object txtCompras8Credito: TEdit
        Left = 125
        Top = 103
        Width = 121
        Height = 21
        TabOrder = 4
        OnExit = txtCompras8CreditoExit
        OnKeyDown = txtCompras8CreditoKeyDown
      end
      object txtCompras8Contado: TEdit
        Left = 264
        Top = 103
        Width = 121
        Height = 21
        TabOrder = 5
        OnExit = txtCompras8ContadoExit
        OnKeyDown = txtCompras8ContadoKeyDown
      end
    end
    object GroupBox1: TGroupBox
      Left = 24
      Top = 271
      Width = 438
      Height = 127
      Caption = 'IVA'
      TabOrder = 1
      object Label4: TLabel
        Left = 32
        Top = 43
        Width = 90
        Height = 13
        Caption = 'Pendiente de pago'
      end
      object Label5: TLabel
        Left = 32
        Top = 78
        Width = 36
        Height = 13
        Caption = 'Pagado'
      end
      object txtIVAPendiente: TEdit
        Left = 131
        Top = 40
        Width = 121
        Height = 21
        TabOrder = 0
        OnExit = txtIVAPendienteExit
        OnKeyDown = txtIVAPendienteKeyDown
      end
      object txtIVAPagado: TEdit
        Left = 131
        Top = 77
        Width = 121
        Height = 21
        TabOrder = 1
        OnExit = txtIVAPagadoExit
        OnKeyDown = txtIVAPagadoKeyDown
      end
    end
    object btnGuardar: TButton
      Left = 208
      Top = 456
      Width = 75
      Height = 25
      Caption = 'Guardar'
      TabOrder = 3
      OnClick = btnGuardarClick
    end
    object txtDescuento: TEdit
      Left = 123
      Top = 77
      Width = 121
      Height = 21
      TabOrder = 2
      OnExit = txtDescuentoExit
      OnKeyDown = txtDescuentoKeyDown
    end
    object txtCtaProveedores: TEdit
      Left = 123
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 4
      OnExit = txtCtaProveedoresExit
      OnKeyDown = txtCtaProveedoresKeyDown
    end
    object txtCtaIEPS: TEdit
      Left = 123
      Top = 421
      Width = 121
      Height = 21
      TabOrder = 5
      OnExit = txtCtaIEPSExit
      OnKeyDown = txtCtaIEPSKeyDown
    end
    object cbIgnorar: TCheckBox
      Left = 296
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Ignorar'
      TabOrder = 6
    end
  end
  object txtBuscador: TEdit
    Left = 80
    Top = 20
    Width = 193
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    OnChange = txtBuscadorChange
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'DriverID=FB')
    LoginPrompt = False
    Left = 280
    Top = 208
  end
  object qryProveedores: TFDQuery
    AfterOpen = qryProveedoresAfterOpen
    AfterScroll = qryProveedoresAfterScroll
    Connection = Conexion
    SQL.Strings = (
      'select tercero_co_id as proveedor_id,nombre'
      'from terceros_co'
      'where es_proveedor= '#39'S'#39
      'order by nombre')
    Left = 280
    Top = 272
  end
  object dsClientes: TDataSource
    DataSet = qryProveedores
    Left = 280
    Top = 168
  end
end
