unit ULicencia;

interface

uses
    WbemScripting_TLB,
    system.Variants,Registry,System.SysUtils,UEnvVars,Windows, Vcl.Dialogs, ActiveX,system.IOutils,
    IdCoderMIME, TntLXUtils;

type TLicencia = class

    private
    public
    Function GetMotherBoardSerial:string;
    Function GetPCModel:string;
    function Decrypt (const s: string; Key: Word) : string;
    function Encrypt (const s: string; Key: Word) : string;
    function Encrypt2(const InString:string; Salt:string): string;
    function Decrypt2(const InString:string; Salt:string): string;
    function StringToWord(const st:string):word;
    function SetGlobalEnvironment(const Name, Value: string;  const User: Boolean = True): Boolean;
    function GetEnvVariable(Name: string; User: Boolean = True): string;
    function GetHDDSerialNumber : string;
    function AES128_Encrypt(Value, Password: string): string;
    function TLicencia.AES128_Decrypt

    end;


implementation

//-------------------------------------------------------------------------------------------------------------------------
//    Base64 Encode/Decode
//-------------------------------------------------------------------------------------------------------------------------

function Base64_Encode(Value: TBytes): string;
var
  Encoder: TIdEncoderMIME;
begin
  Encoder := TIdEncoderMIME.Create(nil);
  try
    Result := Encoder.EncodeBytes(Value);
  finally
    Encoder.Free;
  end;
end;

function Base64_Decode(Value: string): TBytes;
var
  Encoder: TIdDecoderMIME;
begin
  Encoder := TIdDecoderMIME.Create(nil);
  try
    Result := Encoder.DecodeBytes(Value);
  finally
    Encoder.Free;
  end;
end;

//-------------------------------------------------------------------------------------------------------------------------
//    WinCrypt.h
//-------------------------------------------------------------------------------------------------------------------------

type
  HCRYPTPROV  = Cardinal;
  HCRYPTKEY   = Cardinal;
  ALG_ID      = Cardinal;
  HCRYPTHASH  = Cardinal;

const
  _lib_ADVAPI32    = 'ADVAPI32.dll';
  CALG_SHA_256     = 32780;
  CALG_AES_128     = 26126;
  CRYPT_NEWKEYSET  = $00000008;
  PROV_RSA_AES     = 24;
  KP_MODE          = 4;
  CRYPT_MODE_CBC   = 1;

function CryptAcquireContext(var Prov: HCRYPTPROV; Container: PChar; Provider: PChar; ProvType: LongWord; Flags: LongWord): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptAcquireContextW';
function CryptDeriveKey(Prov: HCRYPTPROV; Algid: ALG_ID; BaseData: HCRYPTHASH; Flags: LongWord; var Key: HCRYPTKEY): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptDeriveKey';
function CryptSetKeyParam(hKey: HCRYPTKEY; dwParam: LongInt; pbData: PBYTE; dwFlags: LongInt): LongBool stdcall; stdcall; external _lib_ADVAPI32 name 'CryptSetKeyParam';
function CryptEncrypt(Key: HCRYPTKEY; Hash: HCRYPTHASH; Final: LongBool; Flags: LongWord; pbData: PBYTE; var Len: LongInt; BufLen: LongInt): LongBool;stdcall;external _lib_ADVAPI32 name 'CryptEncrypt';
function CryptDecrypt(Key: HCRYPTKEY; Hash: HCRYPTHASH; Final: LongBool; Flags: LongWord; pbData: PBYTE; var Len: LongInt): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptDecrypt';
function CryptCreateHash(Prov: HCRYPTPROV; Algid: ALG_ID; Key: HCRYPTKEY; Flags: LongWord; var Hash: HCRYPTHASH): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptCreateHash';
function CryptHashData(Hash: HCRYPTHASH; Data: PChar; DataLen: LongWord; Flags: LongWord): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptHashData';
function CryptReleaseContext(hProv: HCRYPTPROV; dwFlags: LongWord): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptReleaseContext';
function CryptDestroyHash(hHash: HCRYPTHASH): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptDestroyHash';
function CryptDestroyKey(hKey: HCRYPTKEY): LongBool; stdcall; external _lib_ADVAPI32 name 'CryptDestroyKey';

//-------------------------------------------------------------------------------------------------------------------------

{$WARN SYMBOL_PLATFORM OFF}

function __CryptAcquireContext(ProviderType: Integer): HCRYPTPROV;
begin
  if (not CryptAcquireContext(Result, nil, nil, ProviderType, 0)) then
  begin
    if HRESULT(GetLastError) = NTE_BAD_KEYSET then
      Win32Check(CryptAcquireContext(Result, nil, nil, ProviderType, CRYPT_NEWKEYSET))
    else
      RaiseLastOSError;
  end;
end;

function __AES128_DeriveKeyFromPassword(m_hProv: HCRYPTPROV; Password: string): HCRYPTKEY;
var
  hHash: HCRYPTHASH;
  Mode: DWORD;
begin
  Win32Check(CryptCreateHash(m_hProv, CALG_SHA_256, 0, 0, hHash));
  try
    Win32Check(CryptHashData(hHash, PChar(Password), Length(Password) * SizeOf(Char), 0));
    Win32Check(CryptDeriveKey(m_hProv, CALG_AES_128, hHash, 0, Result));
    // Wine uses a different default mode of CRYPT_MODE_EBC
    Mode := CRYPT_MODE_CBC;
    Win32Check(CryptSetKeyParam(Result, KP_MODE, Pointer(@Mode), 0));
  finally
    CryptDestroyHash(hHash);
  end;
end;

function TLicencia.AES128_Encrypt(Value, Password: string): string;
var
  hCProv: HCRYPTPROV;
  hKey: HCRYPTKEY;
  lul_datalen: Integer;
  lul_buflen: Integer;
  Buffer: TBytes;
begin
  Assert(Password <> '');
  if (Value = '') then
    Result := ''
  else begin
    hCProv := __CryptAcquireContext(PROV_RSA_AES);
    try
      hKey := __AES128_DeriveKeyFromPassword(hCProv, Password);
      try
        // allocate buffer space
        lul_datalen := Length(Value) * SizeOf(Char);
        Buffer := TEncoding.Unicode.GetBytes(Value + '        ');
        lul_buflen := Length(Buffer);
        // encrypt to buffer
        Win32Check(CryptEncrypt(hKey, 0, True, 0, @Buffer[0], lul_datalen, lul_buflen));
        SetLength(Buffer, lul_datalen);
        // base 64 result
        Result := Base64_Encode(Buffer);
      finally
        CryptDestroyKey(hKey);
      end;
    finally
      CryptReleaseContext(hCProv, 0);
    end;
  end;
end;

function TLicencia.AES128_Decrypt(Value, Password: string): string;
var
  hCProv: HCRYPTPROV;
  hKey: HCRYPTKEY;
  lul_datalen: Integer;
  Buffer: TBytes;
begin
  Assert(Password <> '');
  if Value = '' then
    Result := ''
  else begin
    hCProv := __CryptAcquireContext(PROV_RSA_AES);
    try
      hKey := __AES128_DeriveKeyFromPassword(hCProv, Password);
      try
        // decode base64
        Buffer := Base64_Decode(Value);
        // allocate buffer space
        lul_datalen := Length(Buffer);
        // decrypt buffer to to string
        Win32Check(CryptDecrypt(hKey, 0, True, 0, @Buffer[0], lul_datalen));
        Result := TEncoding.Unicode.GetString(Buffer, 0, lul_datalen);
      finally
        CryptDestroyKey(hKey);
      end;
    finally
      CryptReleaseContext(hCProv, 0);
    end;
  end;
end;


function TLicencia.GetHDDSerialNumber : string;
var
  VolumeSerialNumber : DWORD;
  MaximumComponentLength : DWORD;
  FileSystemFlags : DWORD;
  TheSerialNumber : String;
begin
  if GetVolumeInformation(PWidechar(ExtractFileDrive(TPath.GetHomePath)+'\') ,nil,0,@VolumeSerialNumber,
     MaximumComponentLength,FileSystemFlags,nil,0)
     then
     begin
     TheSerialNumber := IntToHex(HiWord(VolumeSerialNumber), 4) +
                        IntToHex(LoWord(VolumeSerialNumber), 4);
     end;
     Result := TheSerialNumber;
end;


Function  TLicencia.GetMotherBoardSerial:string;
var
  WMIServices : ISWbemServices;
  Root        : ISWbemObjectSet;
  Item        : Variant;
  i: Integer;
  SWbemObjectSet  : ISWbemObjectSet;

  SObject         : ISWbemObject;
  Enum            : IEnumVariant;
  TempObj         : OleVariant;
  Value           : Cardinal;
  SWbemPropertySet: ISWbemPropertySet;
begin

  WMIServices := CoSWbemLocator.Create.ConnectServer('.', 'root\cimv2','', '', '', '', 0, nil);
  //Root  := WMIServices.ExecQuery('Select SerialNumber From Win32_BaseBoard','WQL', 0, nil);
  Root  := WMIServices.ExecQuery('SELECT SerialNumber FROM Win32_PhysicalMedia','WQL', 0, nil);
  //showmessage(inttostr(root.count));
  for i := 0 to (Root.Count -1)  do
  begin
    Item := Root.ItemIndex(i);
    if Item.SerialNumber <> null
    then Result:=VarToStr(Item.SerialNumber);
  end;
end;

Function  TLicencia.GetPCModel:string;
var
  WMIServices : ISWbemServices;
  Root        : ISWbemObjectSet;
  Item        : Variant;
  i: Integer;
begin

  WMIServices := CoSWbemLocator.Create.ConnectServer('.', 'root\cimv2','', '', '', '', 0, nil);
  //Root  := WMIServices.ExecQuery('Select SerialNumber From Win32_BaseBoard','WQL', 0, nil);
  Root  := WMIServices.ExecQuery('SELECT model FROM Win32_computersystem','WQL', 0, nil);
  //showmessage(inttostr(root.count));
  for i := 0 to (Root.Count -1)  do
  begin
    Item := Root.ItemIndex(i);
    if Item.SerialNumber <> null
    then Result:=VarToStr(Item.SerialNumber);
  end;
end;




const
  c1 = 52845;
  c2 = 22719;

function TLicencia.Encrypt2(const InString:string; Salt:string): string;
var
  i : Byte;
  StartKey, MultKey, AddKey: Word;
begin
  Result := '';
  if (Salt = '') then begin
    Result := InString;
  end
  else begin
    StartKey := Length(Salt);
    MultKey := Ord(Salt[1]);
    AddKey := 0;
    for i := 1 to Length(Salt) - 1 do AddKey := AddKey + Ord(Salt[i]);
    for i := 1 to Length(InString) do
    begin
      Result := Result + CHAR(Byte(InString[i]) xor (StartKey shr 8));
      StartKey := (Byte(Result[i]) + StartKey) * MultKey + AddKey;
    end;
  end;
end;

function TLicencia.Decrypt2(const InString:string; Salt:string): string;
var
  i : Byte;
  StartKey, MultKey, AddKey: Word;
begin
  Result := '';
  if (Salt = '') then begin
    Result := InString;
  end
  else begin
    StartKey := Ord(Salt[1]);
    MultKey := Length(Salt);
    AddKey := 0;
    for i := 1 to Length(Salt) - 1 do AddKey := AddKey + Ord(Salt[i]);
    for i := 1 to Length(InString) do
    begin
      Result := Result + CHAR(Byte(InString[i]) xor (StartKey shr 8));
      StartKey := (Byte(InString[i]) + StartKey) * MultKey + AddKey;
    end;
  end;
end;


function TLicencia.Encrypt (const s: string; Key: Word) : string;
var
  i : byte;
begin
  //  Result[0] := s[0];
  SetLength(Result, length(s));
  for i := 1 to length (s) do
    begin
      Result[i] := Char (byte (s[i]) xor (Key shr 8));
      Key := (byte (Result[i]) + Key) * c1 + c2
    end
end;


function TLicencia.Decrypt (const s: string; Key: Word) : string;
var
  i : byte;
begin
  //Result[0] := s[0];
  SetLength(Result, length(s));
  for i := 1 to length (s) do
    begin
      Result[i] := Char (byte (s[i]) xor (Key shr 8));
      Key := (byte (s[i]) + Key) * c1 + c2
    end
end;

function TLicencia.StringToWord(const st:string):word;
var
  i:integer;
begin
  Result:= 0;
  for i := 0 to length(st)+1 do
    begin
       Result := Result + ord(st[i]);
    end;
end;


function TLicencia.SetGlobalEnvironment(const Name, Value: string;  const User: Boolean = True): Boolean;
resourcestring
  REG_MACHINE_LOCATION = 'System\CurrentControlSet\Control\Session Manager\Environment';
  REG_USER_LOCATION = 'Environment';
  var
  env : TEnvVar;
begin
env := TEnvVar.create;
  with TRegistry.Create do
    try
      Result := OpenKey(REG_USER_LOCATION, True);
      if Result then
      begin
        WriteString(Name, Value); { Write Registry for Global Environment }
        { Update Current Process Environment Variable }
        env.SetEnvVarValue(PChar(Name), PChar(Value));
        { Send Message To All Top Window for Refresh }
        //SendMessage(HWND_BROADCAST, WM_SETTINGCHANGE, 0, Integer(PChar('Environment')));
      end;
    finally
      Free;
    end;
end;

function TLicencia.GetEnvVariable(Name: string; User: Boolean = True): string;
var
  Str: array[0..255] of char;
begin
  with TRegistry.Create do
  try
//    if User then
    begin
      RootKey := HKEY_CURRENT_USER;
      //OpenKey('Environment', False);
      OpenKeyReadOnly('Environment');
    end;
//    else
//    begin
//      RootKey := HKEY_LOCAL_MACHINE;
//      //OpenKey('SYSTEM\CurrentControlSet\Control\Session ' +
//      //  'Manager\Environment', False);
//      OpenKeyReadOnly('SYSTEM\CurrentControlSet\Control\Session ' +
//        'Manager\Environment', False);
//    end;
    Result := ReadString(Name);
    ExpandEnvironmentStrings(PChar(Result), Str, 255);
    Result := Str;
  finally
    Free;
  end;
end;




end.
