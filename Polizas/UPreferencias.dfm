object FConfig: TFConfig
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Preferencias'
  ClientHeight = 202
  ClientWidth = 477
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 33
    Top = 11
    Width = 112
    Height = 13
    Caption = 'Cuenta General Bancos'
  end
  object txtCtaBancos: TEdit
    Left = 161
    Top = 8
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 0
    OnKeyDown = txtCtaBancosKeyDown
  end
  object btnGuardar: TButton
    Left = 155
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Guardar'
    TabOrder = 1
    OnClick = btnGuardarClick
  end
  object btnSalir: TButton
    Left = 275
    Top = 168
    Width = 75
    Height = 25
    Caption = 'Salir'
    ModalResult = 2
    TabOrder = 2
  end
  object gbIVAClientes: TGroupBox
    Left = 24
    Top = 35
    Width = 438
    Height = 127
    Caption = 'IVA'
    TabOrder = 3
    object Label5: TLabel
      Left = 32
      Top = 43
      Width = 90
      Height = 13
      Caption = 'Pendiente de pago'
    end
    object Label11: TLabel
      Left = 32
      Top = 78
      Width = 36
      Height = 13
      Caption = 'Pagado'
    end
    object txtIVAPendiente: TEdit
      Left = 131
      Top = 40
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      OnExit = txtIVAPendienteExit
      OnKeyDown = txtIVAPendienteKeyDown
    end
    object txtIVAPagado: TEdit
      Left = 131
      Top = 77
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      OnExit = txtIVAPagadoExit
      OnKeyDown = txtIVAPagadoKeyDown
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'Password=masterkey'
      'User_Name=sysdba'
      'DriverID=FB')
    LoginPrompt = False
    Left = 26
    Top = 608
  end
  object qryClientes: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select tercero_co_id as cliente_id,nombre'
      'from terceros_co'
      'where es_cliente = '#39'S'#39
      'order by nombre')
    Left = 72
    Top = 608
  end
  object qryProveedores: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select tercero_co_id as proveedor_id,nombre'
      'from terceros_co'
      'where es_proveedor= '#39'S'#39
      'order by nombre')
    Left = 104
    Top = 608
  end
end
