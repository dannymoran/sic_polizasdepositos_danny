unit UPassword;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TPassword = class(TForm)
    txtPassword: TEdit;
    lblCaption: TLabel;
    btnAceptar: TButton;
    procedure btnCancelarClick(Sender: TObject);
    procedure btnAceptarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Password: TPassword;

implementation

{$R *.dfm}

procedure TPassword.btnAceptarClick(Sender: TObject);
begin
  modalResult := mrOK;
end;

procedure TPassword.btnCancelarClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

end.
