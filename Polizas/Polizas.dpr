program Polizas;

uses
  Vcl.Forms,
  UClientes in 'UClientes.pas' {FClientes},
  UPrincipal in 'UPrincipal.pas' {FPrincipal},
  UPlantillas in 'UPlantillas.pas' {FPlantillas},
  UCuentasCo in 'UCuentasCo.pas' {FCuentas},
  UPreferencias in 'UPreferencias.pas' {FConfig},
  UConexiones in 'UConexiones.pas' {Conexiones},
  UEnvVars in 'UEnvVars.pas',
  ULicenciaProv in 'ULicenciaProv.pas' {FormLicenciaProv},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  ULicencia in 'ULicencia.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  Vcl.Themes,
  Vcl.Styles,
  UVariables in 'UVariables.pas' {FVariables},
  UProveedores in 'UProveedores.pas' {FProveedores},
  UCambiaEmpresas in 'UCambiaEmpresas.pas' {FCambiaEmpresas},
  ULicenciaArchivo in 'ULicenciaArchivo.pas' {FLicenciaArchivo},
  Sysutils,
  System.ioutils,
  USelEmpresasLicencia in 'USelEmpresasLicencia.pas' {FSelEmpresasLicencia},
  UPassword in 'UPassword.pas' {Password},
  UFormula in 'UFormula.pas' {Formula},
  UCuentas in 'UCuentas.pas' {Cuentas};

{$R *.res}
var
  lic : TLicencia;
  pwd : WideString;
begin
  lic := TLicencia.Create;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Cyan Dusk');
  pwd := GetEnvironmentVariable('sic_polizas_pwd');
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TFormula, Formula);
  Application.CreateForm(TCuentas, Cuentas);
  if pwd <> '' then
  begin
    if lic.DecryptStr(pwd,lic.stringtoword('enc')).Split([';'])[1] = trim(lic.GetHddSerialNumber) then
    begin
      Application.CreateForm(TLogin, Login);
     //Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
  end;
  end
  else
  begin
    Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
  end;
 // Application.CreateForm(TFLicenciaArchivo, FLicenciaArchivo);
  Application.Run;
end.
